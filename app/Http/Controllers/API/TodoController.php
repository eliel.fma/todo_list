<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use Carbon\Carbon;
use Validator;
use DB;

use App\Models\Todo;

use App\Http\Resources\Todo as TodoResource;

class TodoController extends BaseController
{

    /**
     * Display a listing of the resource.
     * @author elielfernandes
     * @version 1.0.0
     * @return  \Illuminate\Http\Response
     */
    public function index(){
        $usr = \Auth::user();
        $todos = Todo::where('user_id', $usr->id)->get();
        return $this->sendResponse(TodoResource::collection($todos), 'Products retrieved successfully.');
    }

    /**
     * Store new or updated To Do Items
     * @author elielfernandes
     * @version 1.0.0
     */
    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'title'   => 'required|min:3',
            'details' => 'required',
            'file'    => 'mimes:doc,docx,pdf,txt|max:2048',
        ]);
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $usr = \Auth::user();
        /*
            Reminders
            0 - No reminder
            1 - One day before
            2 - One week before
            3 - Two week before
            4 - One hour before
         */
        $dt = NULL;
        if(!is_null($request->duetime)){
            $dt = Carbon::parse($request->duetime);
            switch($request->reminder){
                case 1:
                    $dt->sub('1 day');
                    break;
                case 2:
                    $dt->sub('7 days');
                    break;
                case 3:
                    $dt->sub('14 days');
                    break;
                case 4:
                    $dt->sub('1 hour');
                    break;
                default:
                    break;
            }
        }
        
        $item = Todo::findOrNew($request->id);
        
        $file = NULL;
        if($files = $request->file('file')){
            if(!is_null($request->attachment) || $request->attachment != ''){
                \Storage::delete($request->attachment);
            }
            $file = $request->file->store('public/documents');
        }
        $item->user_id    = $usr->id;
        $item->title      = $request->title;
        $item->details    = $request->details;
        $item->duedate    = $dt;
        $item->complete   = ($request->complete) ? $request->complete : 0;
        $item->reminder   = $request->reminder_type;
        $item->attachment = $file;
        $item->save();

        if(!is_null($request->id)){
            return $this->sendResponse(new TodoResource($item), 'Item updated successfully.');
        }else{
            return $this->sendResponse(new TodoResource($item), 'Item created successfully.');
        }
    }

    /**
     * Show Created Item Info
     * @author elielfernandes
     * @version 1.0.0
     */
    public function show($id){
        $item = Todo::find($id);

        if(is_null($item)){
            return $this->sendError('Item not found');
        }
        $a = str_replace('public', '', $item->attachment);
        $item->attachment = asset('storage/' . $a);

        return $this->sendResponse(new TodoResource($item), 'Item retrieved successfully.');
    }

    /**
     * Toggle Complete status to Complete or Uncomplete
     * @author elielfernandes
     * @version 1.0.0
     * @param   int  $id    ID of the item
     */
    public function toggleComplete($id){
        $item = Todo::find($id);
        $a = ($item->complete == 0) ? 1 : 0;
        $item->complete = $a;
        $item->save();
        $arr = array('complete' => $a);
        return $this->sendResponse($arr, 'Item changed status successfully.');
    }

    /**
     * Remove the specified item
     * @author elielfernandes
     * @version 1.0.0
     */
    public function destroy($id){
        $item = Todo::find($id);
        if(!is_null($item->attachment) || $item->attachment != ''){
            \Storage::delete($item->attachment);
        }
        $item->delete();

        return $this->sendResponse([], 'Item deleted successfully.');
    }
}
