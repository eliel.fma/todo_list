<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BaseController extends Controller
{
    /**
     * success response method.
     * @author elielfernandes
     * @version 1.0.0
     * @param   data    $result     Object with data return
     * @param   string  $message    String with data answer
     * @return  \Illuminate\Http\Response
     */
    public function sendResponse($result, $message){
        $response = [
            'success' => true,
            'data'    => $result,
            'message' => $message,
        ];

        return response()->json($response, 200);
    }

    /**
     * return error response.
     * @author elielfernandes
     * @version 1.0.0
     * @param   string  $error          String with error text
     * @param   array   $errorMessages  Array with error messages provided
     * @param   integer $code           Code to return browser error
     * @return  \Illuminate\Http\Response
     */
    public function sendError($error, $errorMessages = [], $code = 404){
        $response = [
            'success' => false,
            'message' => $error,
        ];

        if(!empty($errorMessages)){
            $response['data'] = $errorMessages;
        }

        return response()->json($response, $code);
    }
}
