<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Todo extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'id'              => $this->id,
            'title'           => $this->title,
            'details'         => $this->details,
            'duedate'         => $this->duedate,
            'complete'        => $this->complete,
            'reminder_type'   => $this->reminder_type,
            'reminder'        => $this->reminder,
            'attachment'      => $this->attachment,
            'created_at'      => $this->created_at->format('d/m/Y'),
            'updated_at'      => $this->updated_at->format('d/m/Y'),
        ];
    }
}
