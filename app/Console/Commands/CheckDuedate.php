<?php

namespace App\Console\Commands;

use DB;
use App\Notifications\NotifyDueDate;
use Illuminate\Console\Command;

class CheckDuedate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:checkduedate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check database for To Do Items with due date and send mail to remember';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $reminders = DB::table('todos')
                    ->where('reminder_type', '>', '0')
                    ->where('complete', '=', '0')
                    ->get();

        $now = \Carbon\Carbon::now();
        foreach($reminders as $reminder){
            $cd = '';
            $dt = \Carbon\Carbon::parse($reminder->duedate)->format('Y-m-d H:i');
            switch($reminder->reminder_type){
                case 1:
                    $cd = \Carbon\Carbon::parse($now)->addDay()->format('Y-m-d H:i');
                    break;
                case 2:
                    $cd = \Carbon\Carbon::parse($now)->addWeek()->format('Y-m-d H:i');
                    break;
                case 3:
                    $cd = \Carbon\Carbon::parse($now)->addWeeks(2)->format('Y-m-d H:i');
                    break;
                case 4:
                    $cd = \Carbon\Carbon::parse($now)->addHour()->format('Y-m-d H:i');
                    break;
                default:
                    break;
            }
            echo $reminder->title;
            if($now == $cd){
                $usr = User::find($reminder->user_id);
                $usr->notify(new NotifyDueDate($reminder->title, $usr->name));
            }
        }
        //return true;
    }
}
