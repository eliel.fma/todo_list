<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <title>To Do List</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="{{ url('css/app.css') }}">
        <!-- Styles -->
        <style>
            /*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */html{line-height:1.15;-webkit-text-size-adjust:100%}body{margin:0}a{background-color:transparent}[hidden]{display:none}html{font-family:system-ui,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol,Noto Color Emoji;line-height:1.5}*,:after,:before{box-sizing:border-box;border:0 solid #e2e8f0}a{color:inherit;text-decoration:inherit}svg,video{display:block;vertical-align:middle}video{max-width:100%;height:auto}.bg-white{--bg-opacity:1;background-color:#fff;background-color:rgba(255,255,255,var(--bg-opacity))}.bg-gray-100{--bg-opacity:1;background-color:#f7fafc;background-color:rgba(247,250,252,var(--bg-opacity))}.border-gray-200{--border-opacity:1;border-color:#edf2f7;border-color:rgba(237,242,247,var(--border-opacity))}.border-t{border-top-width:1px}.flex{display:flex}.grid{display:grid}.hidden{display:none}.items-center{align-items:center}.justify-center{justify-content:center}.font-semibold{font-weight:600}.h-5{height:1.25rem}.h-8{height:2rem}.h-16{height:4rem}.text-sm{font-size:.875rem}.text-lg{font-size:1.125rem}.leading-7{line-height:1.75rem}.mx-auto{margin-left:auto;margin-right:auto}.ml-1{margin-left:.25rem}.mt-2{margin-top:.5rem}.mr-2{margin-right:.5rem}.ml-2{margin-left:.5rem}.mt-4{margin-top:1rem}.ml-4{margin-left:1rem}.mt-8{margin-top:2rem}.ml-12{margin-left:3rem}.-mt-px{margin-top:-1px}.max-w-6xl{max-width:72rem}.min-h-screen{min-height:100vh}.overflow-hidden{overflow:hidden}.p-6{padding:1.5rem}.py-4{padding-top:1rem;padding-bottom:1rem}.px-6{padding-left:1.5rem;padding-right:1.5rem}.pt-8{padding-top:2rem}.fixed{position:fixed}.relative{position:relative}.top-0{top:0}.right-0{right:0}.shadow{box-shadow:0 1px 3px 0 rgba(0,0,0,.1),0 1px 2px 0 rgba(0,0,0,.06)}.text-center{text-align:center}.text-gray-200{--text-opacity:1;color:#edf2f7;color:rgba(237,242,247,var(--text-opacity))}.text-gray-300{--text-opacity:1;color:#e2e8f0;color:rgba(226,232,240,var(--text-opacity))}.text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}.text-gray-500{--text-opacity:1;color:#a0aec0;color:rgba(160,174,192,var(--text-opacity))}.text-gray-600{--text-opacity:1;color:#718096;color:rgba(113,128,150,var(--text-opacity))}.text-gray-700{--text-opacity:1;color:#4a5568;color:rgba(74,85,104,var(--text-opacity))}.text-gray-900{--text-opacity:1;color:#1a202c;color:rgba(26,32,44,var(--text-opacity))}.underline{text-decoration:underline}.antialiased{-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.w-5{width:1.25rem}.w-8{width:2rem}.w-auto{width:auto}.grid-cols-1{grid-template-columns:repeat(1,minmax(0,1fr))}@media (min-width:640px){.sm\:rounded-lg{border-radius:.5rem}.sm\:block{display:block}.sm\:items-center{align-items:center}.sm\:justify-start{justify-content:flex-start}.sm\:justify-between{justify-content:space-between}.sm\:h-20{height:5rem}.sm\:ml-0{margin-left:0}.sm\:px-6{padding-left:1.5rem;padding-right:1.5rem}.sm\:pt-0{padding-top:0}.sm\:text-left{text-align:left}.sm\:text-right{text-align:right}}@media (min-width:768px){.md\:border-t-0{border-top-width:0}.md\:border-l{border-left-width:1px}.md\:grid-cols-2{grid-template-columns:repeat(2,minmax(0,1fr))}}@media (min-width:1024px){.lg\:px-8{padding-left:2rem;padding-right:2rem}}@media (prefers-color-scheme:dark){.dark\:bg-gray-800{--bg-opacity:1;background-color:#2d3748;background-color:rgba(45,55,72,var(--bg-opacity))}.dark\:bg-gray-900{--bg-opacity:1;background-color:#1a202c;background-color:rgba(26,32,44,var(--bg-opacity))}.dark\:border-gray-700{--border-opacity:1;border-color:#4a5568;border-color:rgba(74,85,104,var(--border-opacity))}.dark\:text-white{--text-opacity:1;color:#fff;color:rgba(255,255,255,var(--text-opacity))}.dark\:text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}}
        </style>

        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
            .cursor-pointer{
                cursor: pointer !important;
                text-decoration: none !important;
            }
            .del-item{
                color:  rgba(199,199,199,0.5);
            }
            .del-item:hover{
                color:  rgba(199,199,199,0.9);
            }
            .itm-check{
                opacity:  10%;
            }
        </style>
    </head>
    <body class="">
        <div class="container-fluid mx-0">
            <div class="row bg-secondary mb-2">
                <div class="col-3 offset-3 py-2">
                    <span class="display-6 text-white">To Do List</span>
                </div>
                <div class="col-3 text-right pr-4 pt-2">
                    <a href="#" onclick="loginForm(); return false;" class="text-white"><small id="txtLogInOut">Login</small></a>
                </div>
            </div>
        </div>
        <div class="container-fluid mx-0 d-none" id="contentPage">
            <div clas="row ml-0">
                <div class="col-6 offset-3 pt-2">
                    <div class="row aling-items-center">
                        <div class="col-5">
                            Olá, <small><strong id="txtName"></strong></small>
                        </div>
                        <div class="col-7 text-right">
                            <div class="row align-items-center no-gutters">
                                <div class="col-9">
                                    <div class="form-check form-check-inline">
                                        <small class="form-check-label">Filter by:</small>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" onchange="filterTodo(this);" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="1" checked>
                                        <label class="form-check-label" for="inlineRadio1"><small>All</small></label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" onchange="filterTodo(this);" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="2">
                                        <label class="form-check-label" for="inlineRadio2"><small>To do</small></label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" onchange="filterTodo(this);" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="3">
                                        <label class="form-check-label" for="inlineRadio3"><small>Done</small></label>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <button onclick="openNew();return false;" class="btn btn-outline-success btn-sm py-0"><small>New Item</small></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mx-0 mt-1">
                <div class="col-6 offset-3">
                    <div class="card">
                        <div class="row mx-0" id="noItems">
                            <div class="col-12 py-3 text-center text-danger">
                                No Items
                            </div>
                        </div>
                        <ul class="list-group" id="myList"></ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="loginForm" tabindex="-1" role="dialog" aria-labelledby="loginForm" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="loginForm">Login / Register</h5>
                        <button type="button" class="close d-none" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-6">
                                <div class="row">
                                    <div class="col-12">
                                        <small class="text-info">
                                            Have an account? Please LogIn below
                                        </small>
                                    </div>
                                </div>
                                <form action="{{ route('usr.login') }}" data-type="login" method="POST">
                                    <div class="form-group mb-1">
                                        <input name="email" type="email" class="form-control form-control-sm rounded-0" placeholder="Your Email">
                                    </div>
                                    <div class="form-group">
                                        <input name="password" type="password" class="form-control form-control-sm rounded-0" placeholder="Your Password">
                                    </div>
                                    <hr />
                                    <div class="row mt-1 mr-0">
                                        <div class="col-6 pr-0">
                                            <div id="loginLoading" class="spinner-grow spinner-grow-sm d-none" role="status">
                                                <span class="sr-only">Loading...</span>
                                            </div>
                                            <small id="loginErr" class="text-danger d-none">Access Denied!</small>
                                        </div>
                                        <div class="col-6 text-right pr-0">
                                            <button onclick="submitFrm(this); return false;" class="btn py-0 btn-success btn-sm">Login</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-6">
                                <div class="row">
                                    <div class="col-12">
                                        <small class="text-info">
                                            Or Register you account
                                        </small>
                                    </div>
                                </div>
                                <form action="{{ route('usr.register') }}" data-type="register" method="POST">
                                    <div class="form-group mb-1">
                                        <input name="name" type="text" class="form-control form-control-sm rounded-0" placeholder="Your Name" required>
                                    </div>
                                    <div class="form-group mb-1">
                                        <input name="email" type="email" class="form-control form-control-sm rounded-0" placeholder="Your Email" required>
                                    </div>
                                    <div class="form-group mb-1">
                                        <input name="password" type="password" class="form-control form-control-sm rounded-0" placeholder="Your Password" required>
                                    </div>
                                    <div class="form-group">
                                        <input name="c_password" type="password" class="form-control form-control-sm rounded-0" placeholder="Confirm your Password" required>
                                    </div>
                                    <hr />
                                    <div class="row mt-1 mr-0">
                                        <div class="col-6 pr-0">
                                            <div id="registerLoading" class="spinner-grow spinner-grow-sm d-none" role="status">
                                                <span class="sr-only">Loading...</span>
                                            </div>
                                            <small id="registerErr" class="text-danger d-none">Can't Register!</small>
                                        </div>
                                        <div class="col-6 text-right pr-0">
                                            <button onclick="submitFrm(this); return false;" class="btn py-0 btn-success btn-sm">Login</button>
                                        </div>
                                    </div>
                                    <!-- <div class="text-right mt-1">
                                        <button onclick="submitFrm(this); return false;" class="btn py-0 btn-success btn-sm">Register</button>
                                    </div> -->
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="newItemForm" tabindex="-1" role="dialog" aria-labelledby="newItemForm" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="newItemForm">New Item</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12">
                                <form id="frmNewUpdate" action="{{ route('usr.register') }}" method="POST">
                                    <input type="hidden" id="itemId" name="id" />
                                    <div class="form-group mb-1">
                                        <label for="title" class="mb-0"><small>Title<small class="text-danger">*</small></small></label>
                                        <input name="title" id="title" type="text" class="form-control form-control-sm rounded-0" placeholder="Title" required>
                                    </div>
                                    <div class="row">
                                        <div class="col-8">
                                            <div class="form-group mb-1">
                                                <label for="duedate" class="mb-0"><small>Due Date</small></label>
                                                <input name="duedate" id="duedate" type="date" class="form-control form-control-sm rounded-0" placeholder="Due Date">
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="form-group mb-1">
                                                <label for="getHour" class="mb-0"><small>Due Time</small></label>
                                                <input name="getHour" id="getHour" type="time" class="form-control form-control-sm rounded-0" placeholder="Time">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-11">
                                            <div class="form-group mb-1">
                                                <label for="file" class="mb-0"><small>Attachment</small></label>
                                                <input name="file" id="file" type="file" class="form-control form-control-sm rounded-0" placeholder="Attachment">
                                            </div>
                                        </div>
                                        <div class="col-1 pt-4 pl-0">
                                            <a id="attDownload" href="#" class="pt-4 cursor-pointer" download>
                                                <i class="fas fa-paperclip"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="reminder" class="mb-0"><small>Reminder</small></label>
                                        <select name="reminder" id="reminder" class="form-control form-control-sm rounded-0">
                                            <option value="0">No reminder</option>
                                            <option value="1">One day before</option>
                                            <option value="2">One week before</option>
                                            <option value="3">Two week before</option>
                                            <option value="4">One hour before</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="details" class="mb-0"><small>Details<small class="text-danger">*</small></small></label>
                                        <textarea name="details" id="details" class="form-control form-control-sm rounded-0" rows="5" required></textarea>
                                    </div>
                                    <hr />
                                    <div class="row mt-1 mr-0">
                                        <div class="col-6 pr-0">
                                            <div id="newLoading" class="spinner-grow spinner-grow-sm d-none" role="status">
                                                <span class="sr-only">Loading...</span>
                                            </div>
                                            <small id="newErr" class="text-danger d-none"></small>
                                        </div>
                                        <div class="col-6 text-right pr-0">
                                            <button onclick="addItem(this); return false;" class="btn py-0 btn-success btn-sm">Save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="delConfirm" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header d-none">
                        <h5 class="modal-title">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p class="text-center">Do you really want to do it?</p>
                    </div>
                    <div class="modal-footer align-items-center justify-content-center">
                        <button type="button" class="btn btn-secondary bt-sm py-0" data-dismiss="modal">No thanks</button>
                        <button onclick="deleteItem(this, true);" type="button" class="btn btn-danger bt-sm py-0">Yes, I'm sure</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="viewItem" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header py-2">
                        <h5 class="modal-title">Item Info</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12">
                                <small class="text-info">Title:</small> <span id="txtNameEvent"></span>
                                <small><span class="badge badge-pill" id="txtComplete"></span></small>
                                <small class="float-right">
                                    <a href="#" id="txtAttachment" class="_blank" download>
                                        <i class="fas fa-paperclip"></i>
                                    </a>
                                </small>
                                <br />
                                <small class="text-info">Duetime:</small> <span id="txtDuetime"></span><br />
                                <small class="text-info">Reminde me:</small> <span id="txtReminder"></span><br />
                                <small class="text-info">Details:</small><br />
                                <span id="txtDetails"></span>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer align-items-center justify-content-center">
                        <button type="button" class="btn btn-secondary bt-sm py-0" data-dismiss="modal">OK</button>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script>
            const url = window.location + 'api/todo';
            
            /**
             * Send login ou register form data
             * @author elielfernandes
             * @version 1.0.0
             */
            function submitFrm(obj){
                $('#loginLoading').addClass('d-none');
                $('#loginErr').addClass('d-none');
                $('#registerLoading').addClass('d-none');
                $('#registerErr').addClass('d-none');

                let frm = $(obj).closest('form').serialize();
                let action = $(obj).closest('form').attr('action') + '?' + frm;
                let tp  = $(obj).closest('form').attr('data-type');
                
                if(tp == 'login'){
                    $('#loginLoading').removeClass('d-none');
                }else{
                    $('#registerLoading').removeClass('d-none');
                }

                $.post(action, function(data){
                    let info = data.data;
                    $('#txtName').html(info.name);
                    localStorage.setItem('todoToken', info.token);
                    localStorage.setItem('todoName', info.name);
                    $(obj).closest('form').trigger('reset');
                    $('#loginForm').modal('hide');
                    $('#txtLogInOut').html('Logout');
                    loadList();
                })
                .fail(function(dt){
                    if(tp == 'login'){
                        $('#loginLoading').addClass('d-none');
                        $('#loginErr').delay(200).removeClass('d-none');
                    }else{
                        $('#registerLoading').addClass('d-none');
                        $('#registerErr').delay(200).removeClass('d-none');
                    }
                });
            }

            /**
             * Open form New Item
             * @author elielfernandes
             * @version 1.0.0
             */
            function openNew(id = null){
                if(id !== null){
                    $.ajax({
                        headers: {"Authorization": "Bearer " + localStorage.getItem('todoToken')},
                        dataType: "json",
                        method: 'GET',
                        url: url + '/' + id,
                        success: function(data){
                            let item = data.data;
                            console.log(item);
                            $('#itemId').val(item.id);
                            $('#title').val(item.title);
                            let a = item.duedate.split(' ');
                            $('#duedate').val(a[0]);
                            $('#getHour').val(a[1]);
                            $('#attDownload').attr('href', item.attachment);
                            $('#attDownload').removeClass('d-none');
                            $('#reminder').val(item.reminder_type);

                            $('#details').val(item.details);
                        },
                        fail: function(data){
                            console.log(data);
                            alert('Error on load');
                        }
                    });
                }else{
                    $('#itemId').val('');
                    $('#attDownload').addClass('d-none');
                }
                $('#newItemForm').modal('show');
            }

            /**
             * Add / Update Items to database
             * @author elielfernandes
             * @version 1.0.0
             */
            function addItem(obj){
                $('#newLoading').removeClass('d-none');
                $('#newErr').html('');
                $('#newErr').addClass('d-none');

                var frm = $(obj).closest('form');
                var xpt = document.getElementById('frmNewUpdate');
                //console.log(xpt);
                var frmData = new FormData(xpt);
                frmData.append('Title', 'Foi');
                let x = '';
                if(frmData.get('duedate') != ''){
                    x += frmData.get('duedate');
                    if(frmData.get('getHour') != ''){
                        if(frmData.get('getHour').length <= 6){
                            x += ' ' + frmData.get('getHour') + ':00';
                        }
                    }else{
                        x += ' 00:00:00';
                    }
                }
                /**
                 * For Debug purpose
                 */
                /*
                    frmData.append('duetime', x);
                    for (var p of frmData) {
                        console.log(p);
                    }
                */
                $.ajax({
                    headers: {"Authorization": "Bearer " + localStorage.getItem('todoToken')},
                    url: url,
                    method: "POST",
                    enctype: 'multipart/form-data',
                    processData: false,
                    contentType: false,
                    cache: false,
                    dataType: "json",
                    data: frmData,
                    success: function(data){
                        $('#newLoading').addClass('d-none');
                        $('#frmNewUpdate').trigger('reset');
                        $('#newItemForm').modal('hide');
                        loadList();
                    },
                    error: function(data){
                        $('#newLoading').addClass('d-none');
                        var answer = data.responseJSON;
                        if(answer.message == "Validation Error."){
                            $('#newErr').removeClass('d-none');
                            $('#newErr').html(answer.data[Object.keys(answer.data)[0]]);
                        }else{
                            $('#newErr').removeClass('d-none');
                            $('#newErr').html('Error');
                        }
                    }
                });
            }

            /**
             * Open the Login Form
             * @author elielfernandes
             */
            function loginForm(){
                if($('#txtLogInOut').html() == 'Login'){
                    $('#loginForm').modal('show');
                }else{
                    $('#txtLogInOut').html('Login');
                    localStorage.removeItem('todoToken');
                    localStorage.removeItem('todoName');
                    $('#contentPage').addClass('d-none');
                    $('#inlineRadio1').trigger('click');
                    $('#myList').html('');
                }
            }

            /**
             * Filter To do List by Status
             * @author elielfernandes
             * @version 1.0.0
             * @param   {object} obj Filter Type
             */
            function filterTodo(obj){
                $('#myList').children('li.d-none').removeClass('d-none');
                let i = $(obj).val();
                switch(i){
                    case '2':
                        $('#myList').children('li.item-done').addClass('d-none');
                        break;
                    case '3':
                        $('#myList').children('li.item-todo').addClass('d-none');
                        break;
                    default:
                        $('#myList').children('li.d-none').removeClass('d-none');
                        break;
                }
            }

            /**
             * Load To do list from API
             * @author elielfernandes
             * @version 1.0.0
             */
            function loadList(){
                $('#contentPage').removeClass('d-none');
                $('#myList').html('');
                $.ajax({
                    headers: {"Authorization": "Bearer " + localStorage.getItem('todoToken')},
                    dataType: "json",
                    method: 'GET',
                    url: url,
                    success: function(data){
                        let listItems = data.data;
                        if(listItems.length > 0){
                            $('#noItems').addClass('d-none');
                        }
                        listItems.forEach(function(item){
                            let a = (item.complete == 0) ? 'Complete' : 'Uncomplete';
                            let b = (item.complete == 0) ? 'item-todo' : 'item-done';
                            let dv  = '<li class="list-group-item pl-0 ' + b + '" data-id="' + item.id + '">';
                                dv += '<div class="row mx-0">';
                                dv += '<div class="col-10">';
                                dv += '<div class="row">';
                                dv += '<div class="col-12">';
                                dv += '<span class="cursor-pointer" onclick="toggleItem(this);" data-toggle="tooltip" title="' + a + '">';
                                if(item.complete == 0){
                                    dv += '<small><i class="fas fa-check itm-check"></i></small>';
                                }else{
                                    dv += '<small><i class="fas fa-check text-success"></i></small>';
                                }
                                dv += '</span>';
                                dv += '&nbsp;<small>' + item.title + '</small>';
                                dv += '</div>';
                                dv += '</div>';
                                dv += '</div>';
                                dv += '<div class="col-2 align-items-center text-right">';
                                if(item.complete == 0){
                                    dv += '<div onclick="openNew(' + item.id + ');" class="btn btn-link p-0 text-info cursor-pointer" data-toggle="tooltip" title="Edit Item">';
                                    dv += '<small>';
                                    dv += '<i class="fas fa-edit"></i>';
                                    dv += '</small>';
                                    dv += '</div>&nbsp;&nbsp;&nbsp;';
                                }else{
                                    dv += '<div onclick="openNew(' + item.id + ');" class="btn btn-link p-0 text-info cursor-pointer d-none" data-toggle="tooltip" title="Edit Item">';
                                    dv += '<small>';
                                    dv += '<i class="fas fa-edit"></i>';
                                    dv += '</small>';
                                    dv += '</div>&nbsp;&nbsp;&nbsp;';
                                }
                                
                                dv += '<div onclick="openView(' + item.id + ');" class="btn btn-link p-0 text-secondary cursor-pointer" data-toggle="tooltip" title="See details">';
                                dv += '<small>';
                                dv += '<i class="far fa-eye"></i>';
                                dv += '</small>';
                                dv += '</div>';

                                dv += '&nbsp;&nbsp;&nbsp;';
                                dv += '<div onclick="deleteItem(this);" class="btn btn-link p-0 del-item cursor-pointer" data-toggle="tooltip" title="Delete Item">';
                                dv += '<small><i class="fas fa-times"></i></small>';
                                dv += '</div>';
                                dv += '</div>';
                                dv += '</div>';
                                dv += '</li>';
                            $('#myList').append(dv);
                        });
                        $('[data-toggle="tooltip"]').tooltip();
                    }
                });
            }

            /**
             * Change the Item Status between Complete and Uncomplete
             * @author elielfernandes
             * @version 1.0.0
             */
            function toggleItem(obj){
                let i = $(obj).closest('li').attr('data-id');
                let path = '{{ route('todo.toggle', '#') }}';
                path = path.replace('#', i);
                $.ajax({
                    headers: {"Authorization": "Bearer " + localStorage.getItem('todoToken')},
                    dataType: "json",
                    url: path,
                    success: function(data){
                        if(data.data.complete == 1){
                            $(obj).closest('li').removeClass('item-todo');
                            $(obj).closest('li').addClass('item-done');
                            $(obj).attr('data-original-title','Uncomplete');
                            $(obj).children(':first').children('i:first').addClass('text-success');
                            $(obj).children(':first').children('i:first').removeClass('itm-check');
                            $(obj).closest('li').children('div:first').children('div:last').children('div:first').addClass('d-none');
                        }else{
                            $(obj).closest('li').addClass('item-todo');
                            $(obj).closest('li').removeClass('item-done');
                            $(obj).attr('data-original-title','Complete');
                            $(obj).children(':first').children('i:first').addClass('itm-check');
                            $(obj).children(':first').children('i:first').removeClass('text-success');
                            $(obj).closest('li').children('div:first').children('div:last').children('div:first').removeClass('d-none');
                        }
                    }
                });
            }

            /**
             * Delete To Do Item
             * @author elielfernandes
             * @version 1.0.0
             */
            function deleteItem(obj, chk = false){
                var dataItem = new FormData();
                if(chk == true){
                    let i = $('#DeleteMe').attr('data-id');
                    let durl = "{{ route('todo.delete', '#') }}";
                    durl = durl.replace('#', i);
                    console.log(durl);
                    $.ajax({
                        headers: {"Authorization": "Bearer " + localStorage.getItem('todoToken')},
                        url: durl,
                        success: function(data){
                            $('#DeleteMe').remove();
                            $('#delConfirm').modal('hide');
                        },
                        fail: function(data){
                            $('#delConfirm').modal('hide');
                            alert('Error!');
                        }
                    });
                }else{
                    $('#DeleteMe').attr('id', '');
                    $(obj).closest('li').attr('id','DeleteMe');
                    $('#delConfirm').modal('show');
                }
            }

            function openView(id){
                $('#txtComplete').removeClass('badge-secondary');
                $('#txtComplete').removeClass('badge-success');
                $.ajax({
                    headers: {"Authorization": "Bearer " + localStorage.getItem('todoToken')},
                    dataType: "json",
                    method: 'GET',
                    url: url + '/' + id,
                    success: function(data){
                        let item = data.data;
                        console.log(item);
                        $('#viewItem').modal('show');
                        $('#txtNameEvent').html(item.title);
                        $('#txtDuetime').text(item.duedate);
                        if(item.duedate != item.reminder){
                            $('#txtReminder').text(item.reminder);
                        }else{
                            $('#txtReminder').text('No Reminder');
                        }
                        if(item.complete == 0){
                            $('#txtComplete').addClass('badge-secondary');
                            $('#txtComplete').text('Incomplete');
                        }else{
                            $('#txtComplete').addClass('badge-success');
                            $('#txtComplete').text('Complete');
                        }
                        if(item.attachment != ""){
                            $('#txtAttachment').attr('href',item.attachment);
                            $('#txtAttachment').closest('small').removeClass('d-none');
                        }else{
                            $('#txtAttachment').closest('small').addClass('d-none');
                        }
                        $('#txtDetails').text(item.details);
                    },
                    fail: function(data){
                        console.log(data);
                        alert('Error on load');
                    }
                });
            }

            /**
             * Initialize JavaScript page
             * @author elielfernandes
             * @version 1.0.0
             */
            $(function () {
                //$('#viewItem').modal('show');
                $('[data-toggle="tooltip"]').tooltip();

                $('form').submit(function(e){
                    e.preventDefault();
                });

                var chkToken = localStorage.getItem('todoToken');
                if(chkToken){
                    $('#txtLogInOut').html('Logout');
                    $('#txtName').html(localStorage.getItem('todoName'));
                    loadList();
                }
            });
        </script>
    </body>
</html>
