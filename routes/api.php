<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\API\RegisterController;
use App\Http\Controllers\API\TodoController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', [RegisterController::class, 'register'])->name('usr.register');
Route::post('login', [RegisterController::class, 'login'])->name('usr.login');

Route::middleware('auth:api')->group( function() {
    Route::resource('todo', TodoController::class);
    Route::get('todo/delete/{id}', [TodoController::class, 'destroy'])->name('todo.delete');
    Route::get('complete/{id}', [TodoController::class, 'toggleComplete'])->name('todo.toggle');
});
